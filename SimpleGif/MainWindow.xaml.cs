﻿using Microsoft.Win32;
using SimpleGif.ConverterTools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Xabe.FFmpeg;
namespace SimpleGif
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Словарь значений аргументов видеофильтра.
        /// </summary>
        public static Dictionary<string, string> FlipRotateValues = new Dictionary<string, string>
        {
            {String.Empty, "Отсутствует"},
            {"hflip,vflip", "Отраж. по вертик. и гориз."},
            {"hflip", "Отраж. по гориз."},
            {"vflip", "Отраж. по вертик."},
            {"transpose=3", "90° и отражение"},
            {"transpose=0", "-90° и отражение"},
            {"transpose=2", "-90°"},
            {"transpose=1", "90°"}
        };

        /// <summary>
        /// Массив допустимых расширений медиафайла.
        /// </summary>
        public static readonly string[] ValidExtensions = {".mp4", ".avi", ".mov"};

        /// <summary>
        /// Угол поворота исходного файла.
        /// </summary>
        public int RotateDegree { get; set; }

        /// <summary>
        /// Конвертер гифок.
        /// </summary>
        private GifConverter MainGifConverter;

        public MainWindow()
        {
            InitializeComponent();

            // Указываем путь к исполняемым файлам ffmpeg.         
            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "ffmpeg\\ffmpeg.exe")
                || !File.Exists(AppDomain.CurrentDomain.BaseDirectory + "ffmpeg\\ffprobe.exe"))
            {
                MessageBox.Show("Не найдены исполняемые файлы ffmpeg.\r\nПриложение будет закрыто.", "Ошибка"
                    , MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
                return;
            }

            FFmpeg.ExecutablesPath = AppDomain.CurrentDomain.BaseDirectory + "ffmpeg";

            MainGifConverter = new GifConverter();
            MainGifConverter.ConvertError += MainGifConverter_ConvertError;
            MainGifConverter.ConvertSuccess += MainGifConverter_ConvertSuccess;
        }

        /// <summary>
        /// Команда открытия медиафайла.
        /// </summary>
        private async void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                DefaultExt = ".mp4",
                CheckFileExists = true,
                CheckPathExists = true,
                Title = "Выбор видеофайла",
                Filter = "Видео (*.mp4)|*.mp4|Видео (*.avi)|*.avi|Видео (*.mov)|*.mov"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                if (!ValidExtensions.Contains(Path.GetExtension(openFileDialog.FileName)))
                {
                    MessageBox.Show("Неверный формат.", "Выбор видеофайла"
                        , MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                MePlayer.Source = new Uri(openFileDialog.FileName);
                await SetSettingsByMediaInfoAsync(openFileDialog.FileName);
            }
        }

        /// <summary>
        /// Команда запуска проигрывания медиафайла.
        /// </summary>
        private void Play_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MePlayer.Play();
        }

        /// <summary>
        /// Команда приостановки проигрывания медиафайла.
        /// </summary>
        private void Pause_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            MePlayer.Pause();
        }

        /// <summary>
        /// Обработчик события старта перетаскивания слайдера.
        /// </summary>
        private void SliProgress_DragStarted(object sender, DragStartedEventArgs e)
        {
            MePlayer.Pause();
        }

        /// <summary>
        /// Обработчик события открытия медиафайла.
        /// </summary>
        private void MePlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            SliProgress.Minimum = 0;
            SliProgress.Maximum = MePlayer.NaturalDuration.TimeSpan.TotalMilliseconds;
        }

        /// <summary>
        /// Обработчик события конца воспроизведения медиафайла.
        /// </summary>
        private void MePlayer_MediaEnded(object sender, RoutedEventArgs e)
        {
            MePlayer.Stop();
        }

        /// <summary>
        /// Обработчик события изменения позиции медиафайла.
        /// </summary>
        private void MePlayer_PositionChange(object sender, RoutedPropertyChangedEventArgs<TimeSpan> e)
        {
            SliProgress.Value = e.NewValue.TotalMilliseconds;
            lblProgressStatus.Text = e.NewValue.ToString(@"hh\:mm\:ss");
        }

        /// <summary>
        /// Обработчик события изменения значения слайдера.
        /// </summary>
        private void SliProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (MePlayer.IsPlaying || MePlayer.Source == null || !MePlayer.NaturalDuration.HasTimeSpan)
            {
                return;
            }

            MePlayer.Position = TimeSpan.FromMilliseconds(SliProgress.Value);
        }

        /// <summary>
        /// Обработчик события изменения выбранного значения аргумента видеофильтра.
        /// </summary>
        private void FlipRotateComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var transGroup = new TransformGroup();

            switch (FlipRotateComboBox.SelectedValue)
            {
                case "":
                    transGroup.Children.Add(new ScaleTransform());
                    transGroup.Children.Add(new RotateTransform(RotateDegree));
                    MePlayer.LayoutTransform = transGroup;
                    break;

                // Поворот влево на 90 градусов и отражение.
                case "transpose=0":
                    if (this.NeedSwap())
                    {
                        transGroup.Children.Add(new ScaleTransform { ScaleY = -1 });
                    }
                    else
                    {
                        transGroup.Children.Add(new ScaleTransform { ScaleX = -1 });
                    }
                    transGroup.Children.Add(new RotateTransform(270 + RotateDegree));
                    MePlayer.LayoutTransform = transGroup;
                    break;

                // Поворот вправо на 90 градусов.
                case "transpose=1":
                    transGroup.Children.Add(new ScaleTransform());
                    transGroup.Children.Add(new RotateTransform(90 + RotateDegree));
                    MePlayer.LayoutTransform = transGroup;
                    break;

                // Поворот влево на 90 градусов.
                case "transpose=2":
                    transGroup.Children.Add(new ScaleTransform());
                    transGroup.Children.Add(new RotateTransform(270 + RotateDegree));
                    MePlayer.LayoutTransform = transGroup;
                    break;

                // Поворот вправо на 90 градусов и отражение.
                case "transpose=3":
                    if (this.NeedSwap())
                    {
                        transGroup.Children.Add(new ScaleTransform { ScaleY = -1 });
                    }
                    else
                    {
                        transGroup.Children.Add(new ScaleTransform { ScaleX = -1 });
                    }
                    transGroup.Children.Add(new RotateTransform(90 + RotateDegree));
                    MePlayer.LayoutTransform = transGroup;
                    break;

                // Горизонтальное отражение.
                case "hflip":
                    if (this.NeedSwap())
                    {
                        transGroup.Children.Add(new ScaleTransform { ScaleY = -1 });
                    }
                    else
                    {
                        transGroup.Children.Add(new ScaleTransform { ScaleX = -1 });
                    }
                    transGroup.Children.Add(new RotateTransform(RotateDegree));
                    MePlayer.LayoutTransform = transGroup;
                    break;

                // Вертикальное отражение.
                case "vflip":
                    if (this.NeedSwap())
                    {
                        transGroup.Children.Add(new ScaleTransform { ScaleX = -1 });
                    }
                    else
                    {
                        transGroup.Children.Add(new ScaleTransform { ScaleY = -1 });
                    }
                    transGroup.Children.Add(new RotateTransform(RotateDegree));
                    MePlayer.LayoutTransform = transGroup;
                    break;

                // Горизонтальное и вертикальное отражение.
                case "hflip,vflip":
                    transGroup.Children.Add(new ScaleTransform(-1, -1));
                    transGroup.Children.Add(new RotateTransform(RotateDegree));
                    MePlayer.LayoutTransform = transGroup;
                    break;
            }
        }

        /// <summary>
        /// Обработчик события нажатия на клавишу.
        /// </summary>
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Space:
                    if (MePlayer.IsPlaying)
                    {
                        MePlayer.Pause();
                    }
                    else
                    {
                        MePlayer.Play();
                    }
                    break;

                case Key.Left:
                    double pos = MePlayer.Position.TotalMilliseconds - 1000;
                    if (MePlayer.NaturalDuration.HasTimeSpan
                        && pos >= 1000)
                    {
                        MePlayer.Position = TimeSpan.FromMilliseconds(pos);
                    }
                    else
                    {
                        MePlayer.Position = TimeSpan.FromMilliseconds(0);
                    }
                    break;

                case Key.Right:
                    pos = MePlayer.Position.TotalMilliseconds + 1000;
                    if (MePlayer.NaturalDuration.HasTimeSpan
                        && pos <= MePlayer.NaturalDuration.TimeSpan.TotalMilliseconds - 1000)
                    {
                        MePlayer.Position = TimeSpan.FromMilliseconds(pos);
                    }
                    else
                    {
                        MePlayer.Position = MePlayer.NaturalDuration.TimeSpan;
                    }
                    break;
            }
        }

        /// <summary>
        /// Обработчик события кнопки управления конвертацией.
        /// </summary>
        private async void ConvertButton_Click(object sender, RoutedEventArgs e)
        {
            if (MainGifConverter.MedInfo == null)
            {
                return;
            }

            SaveFileDialog saveFD = new SaveFileDialog
            {
                DefaultExt = ".gif",
                AddExtension = true,
                CheckPathExists = true,
                OverwritePrompt = true,
                Title = "Конвертирование анимации",
                Filter = "Анимация (*.gif)|*.gif"
            };

            if (saveFD.ShowDialog() == false)
            {
                return;
            }

            if (Path.GetExtension(saveFD.FileName) != ".gif")
            {
                MessageBox.Show("Неверный формат.", "Конвертирование анимации"
                        , MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            ProgressIndicator.IsIndeterminate = true;
            ConvertButton.IsEnabled = false;

            FFmpegArgs arguments = GetFFmpegArgs();

            await MainGifConverter.ConvertAsync(saveFD.FileName, arguments, SeekTimeSpanUpDown.Value.Value);

            ProgressIndicator.IsIndeterminate = false;
            ConvertButton.IsEnabled = true;
        }

        /// <summary>
        /// Обработчик события кнопки сменить разрешение.
        /// </summary>
        private void SwapScaleButton_Click(object sender, RoutedEventArgs e)
        {
            int temp = WidthIntegerUpDown.Value.Value;

            WidthIntegerUpDown.Value = HeightIntegerUpDown.Value.Value;
            HeightIntegerUpDown.Value = temp;
        }

        /// <summary>
        /// Возвращает true, если видеофайл необходимо перевернуть, иначе false.
        /// </summary>
        private bool NeedSwap()
        {
            int absDegree = Math.Abs(RotateDegree);

            return absDegree == 90 || absDegree == 270 ? true : false;
        }

        /// <summary>
        /// Обработчик события успешной конвертации.
        /// </summary>
        private void MainGifConverter_ConvertSuccess(object sender, ConvertSuccessEventArgs e)
        {
            string message = "Анимация успешно создана!"
                + Environment.NewLine
                + $"Время начала: {e.StartTime.ToString(@"hh\:mm\:ss")}."
                + Environment.NewLine
                + $"Прошло времени: {e.Duration.ToString(@"hh\:mm\:ss")}."
                + Environment.NewLine
                + "Хотите перейти к выходному файлу?";

            string caption = "Конвертация";

            var result =
                MessageBox.Show(message, caption, MessageBoxButton.YesNo, MessageBoxImage.Information);

            if (result == MessageBoxResult.Yes)
            {
                System.Diagnostics.Process.Start("explorer.exe", "/select," + e.SavedFile.FullName);
            }
        }

        /// <summary>
        /// Обработчик события ошибки конвертации.
        /// </summary>
        private void MainGifConverter_ConvertError(object sender, ConvertErrorEventArgs e)
        {
            string message = $"Прошло времени: {e.Duration.ToString(@"hh\:mm\:ss")}"
                + Environment.NewLine + e.Message;
            string caption = "Ошибка конвертации";

            MessageBox.Show(message, caption, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// Устанавливает настройки и ограничения элементов управления,
        /// исходя из медиаинформации файла.
        /// </summary>
        /// <param name="filePath">Путь к медиафайлу.</param>
        private async Task SetSettingsByMediaInfoAsync(string filePath)
        {
            // Получаем информацию о медиа файле.
            MainGifConverter.MedInfo = await MediaInfo.Get(filePath);

            int width = MainGifConverter.MedInfo.VideoStreams.FirstOrDefault().Width;
            int height = MainGifConverter.MedInfo.VideoStreams.FirstOrDefault().Height;
            int max = width > height ? width : height;

            // Устанавливаем свойства для элементов управления.
            FPSIntegerUpDown.Maximum = (int)MainGifConverter.MedInfo.VideoStreams.FirstOrDefault().FrameRate;
            FPSIntegerUpDown.Value = FPSIntegerUpDown.Maximum;
            WidthIntegerUpDown.Maximum = max;
            HeightIntegerUpDown.Maximum = max;         
            SeekTimeSpanUpDown.Maximum = MainGifConverter.MedInfo.Duration;

            // Запрашиваем информацию о градусе поворота.
            string rotateInfo = await Probe.New().Start("-loglevel error -select_streams v:0 -show_entries"
                + $" stream_tags=rotate -of default=nw=1:nk=1 -i {MainGifConverter.MedInfo.FileInfo.FullName}");
            rotateInfo = rotateInfo.Replace(Environment.NewLine, "");

            int.TryParse(rotateInfo, out int degree);

            // Градус потом пригодится, поэтому записываем его в свойство.
            this.RotateDegree = degree;

            // Меняем местами высоту и ширину, соответствуя углу.
            bool needSwap = this.NeedSwap();
            WidthIntegerUpDown.Value = needSwap ? height : width;
            HeightIntegerUpDown.Value = needSwap ? width : height;

            // Сбрасываем трансформации, устанавливая текущий градус поворота.
            var transGroup = new TransformGroup();
            transGroup.Children.Add(new ScaleTransform());
            transGroup.Children.Add(new RotateTransform(RotateDegree));

            MePlayer.LayoutTransform = transGroup;
            FlipRotateComboBox.SelectedValue = String.Empty;
        }

        /// <summary>
        /// Возвращает аргументы FFmpeg полученные из элементов управления формы.
        /// </summary>
        private FFmpegArgs GetFFmpegArgs()
        {
            // Если длительность превышает допустимую, корректируем ее.
            int ms = DurationIntegerUpDown.Value.Value * 10;
            if (SeekTimeSpanUpDown.Value.Value.TotalMilliseconds + ms > MainGifConverter.MedInfo.Duration.TotalMilliseconds)
            {
                ms = (int)(MainGifConverter.MedInfo.Duration.TotalMilliseconds - SeekTimeSpanUpDown.Value.Value.TotalMilliseconds);
            }

            int width = 0;
            int height = 0;

            if (FlipRotateComboBox.SelectedValue.ToString().Contains("transpose"))
            {
                width = HeightIntegerUpDown.Value.Value;
                height = WidthIntegerUpDown.Value.Value;
            }
            else
            {
                width = WidthIntegerUpDown.Value.Value;
                height = HeightIntegerUpDown.Value.Value;
            }
            
            FFmpegArgs args = new FFmpegArgs
            {
                Vf = FlipRotateComboBox.SelectedValue.ToString(),
                Duration = (ms / 10).ToString(),
                FPS = FPSIntegerUpDown.Value.Value.ToString(),
                Scale = new FFmpegArgs.ScaleArg(width, height)
            };

            return args;
        }
    }

    
}
