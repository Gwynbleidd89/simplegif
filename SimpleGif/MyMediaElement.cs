﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SimpleGif
{
    /// <summary>
    /// Выполните шаги 1a или 1b, а затем 2, чтобы использовать этот пользовательский элемент управления в файле XAML.
    ///
    /// Шаг 1a. Использование пользовательского элемента управления в файле XAML, существующем в текущем проекте.
    /// Добавьте атрибут XmlNamespace в корневой элемент файла разметки, где он 
    /// будет использоваться:
    ///
    ///     xmlns:MyNamespace="clr-namespace:SimpleGif"
    ///
    ///
    /// Шаг 1б. Использование пользовательского элемента управления в файле XAML, существующем в другом проекте.
    /// Добавьте атрибут XmlNamespace в корневой элемент файла разметки, где он 
    /// будет использоваться:
    ///
    ///     xmlns:MyNamespace="clr-namespace:SimpleGif;assembly=SimpleGif"
    ///
    /// Потребуется также добавить ссылку из проекта, в котором находится файл XAML,
    /// на данный проект и пересобрать во избежание ошибок компиляции:
    ///
    ///     Щелкните правой кнопкой мыши нужный проект в обозревателе решений и выберите
    ///     "Добавить ссылку"->"Проекты"->[Поиск и выбор проекта]
    ///
    ///
    /// Шаг 2)
    /// Теперь можно использовать элемент управления в файле XAML.
    ///
    ///     <MyNamespace:MyMediaElement/>
    ///
    /// </summary>
    public class MyMediaElement : MediaElement
    {
        /// <summary>
        /// Таймер позиции мультимедиа, отслеживает изменение свойства и геренирует событие.
        /// </summary>
        private DispatcherTimer PositionTimer;

        private TimeSpan OldPosition;

        /// <summary>
        /// Указывавает воспроизводится ли файл мультимедиа на данный момент.
        /// </summary>
        public bool IsPlaying { get; private set; }

        public MyMediaElement() : base()
        {
            PositionTimer = new DispatcherTimer(DispatcherPriority.Normal, this.Dispatcher);
            PositionTimer.Interval = TimeSpan.FromMilliseconds(1);
            PositionTimer.Tick += new EventHandler((object sender, EventArgs e) => 
            {
                if (this.Source == null || this.OldPosition == this.Position || !this.NaturalDuration.HasTimeSpan)
                {
                    return;
                }

                TimeSpan oldValue = this.OldPosition;
                this.OldPosition = this.Position;

                RaiseEvent(new RoutedPropertyChangedEventArgs<TimeSpan>(oldValue, this.Position, PositionChangeEvent));
            });
        }

        static MyMediaElement()
        {
            MyMediaElement.PositionChangeEvent = EventManager.RegisterRoutedEvent("PositionChange"
                , RoutingStrategy.Direct, typeof(RoutedPropertyChangedEventHandler<TimeSpan>), typeof(MyMediaElement));
        }

        public static readonly RoutedEvent PositionChangeEvent;

        public event RoutedPropertyChangedEventHandler<TimeSpan> PositionChange
        {
            add => base.AddHandler(MyMediaElement.PositionChangeEvent, value);
            remove => base.RemoveHandler(MyMediaElement.PositionChangeEvent, value);
        }

        /// <summary>
        /// Воспроизводит файл мультимедиа с текущего положения.
        /// </summary>
        public new void Play()
        {
            if (this.Source == null || this.IsPlaying)
            {
                return;
            }

            this.IsPlaying = true;
            PositionTimer.Start();
            base.Play();       
        }

        /// <summary>
        /// Останавливает воспроизведимое мультимедиа и сбрасывает его для воспроизведения с самого начала.
        /// </summary>
        public new void Stop()
        {
            this.IsPlaying = false;
            PositionTimer.Stop();
            base.Stop();
        }

        /// <summary>
        /// Приостанавливает воспроизведение файла мультимедия в текущем положении.
        /// </summary>
        public new void Pause()
        {
            if (!this.IsPlaying)
            {
                return;
            }

            this.IsPlaying = false;
            PositionTimer.Stop();
            base.Pause();
        }

        /// <summary>
        /// Закрывает файл мультимедиа.
        /// </summary>
        public new void Close()
        {
            this.IsPlaying = false;
            PositionTimer.Stop();
            base.Close();
        }
    }
}
