﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Xabe.FFmpeg;
using Xabe.FFmpeg.Model;
using Xabe.FFmpeg.Streams;

namespace SimpleGif.ConverterTools
{
    public sealed class GifConverter
    {
        /// <summary>
        /// Медиаинформация исходного файла. 
        /// </summary>
        public IMediaInfo MedInfo { get; set; } = null;

        // Токен отмены операции конверсии.
        // public CancellationTokenSource CancelToken { get; set; }

        /// <summary>
        /// Событие ошибки конвертации.
        /// </summary>
        public event EventHandler<ConvertErrorEventArgs> ConvertError;

        /// <summary>
        /// Событие успешной конвертации.
        /// </summary>
        public event EventHandler<ConvertSuccessEventArgs> ConvertSuccess;

        /// <summary>
        /// Операция конверсии.
        /// </summary>
        /// <param name="saveFileName">Имя выходного файла .gif.</param>
        /// <param name="arguments">Аргументы для FFmpeg.</param>
        /// <param name="seekTime">Время начала обрезки.</param>
        public async Task ConvertAsync(string saveFileName, FFmpegArgs arguments, TimeSpan seekTime)
        {
            // Засекаем время.
            TimeSpan startTime = DateTime.Now.TimeOfDay;
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            
            bool savedFileExists = File.Exists(saveFileName);

            // Создаем директорию временных файлов, если таковой нет.
            string tempDir = Path.GetTempPath();
            Directory.CreateDirectory(tempDir);

            // Генерируем имена временных файлов.
            string normalFileName = $"{tempDir}n_{Guid.NewGuid() + MedInfo.FileInfo.Extension}";
            string reverseFileName = $"{tempDir}r_{Guid.NewGuid() + MedInfo.FileInfo.Extension}";
            string concatFileName = $"{tempDir}c_{Guid.NewGuid() + MedInfo.FileInfo.Extension}";
            string paletteFileName = $"{tempDir}p_{Guid.NewGuid()}.png";

            #region Cancellatation Token не работает
            //CancelToken?.Dispose();
            //CancelToken = new CancellationTokenSource();

            //normalConvers.OnProgress += async (sender, args) =>
            //{
            //    await progress?.Dispatcher.InvokeAsync(() => progress.Value = args.Percent);
            //};

            //IConversionResult normalResult = await normalConvers.Start(CancelToken.Token);
            #endregion

            try
            {
                // Конверсия файла без инверсии.
                IVideoStream normalVideo = MedInfo.VideoStreams.FirstOrDefault().SetSeek(seekTime);
                IConversion normalConvers = Conversion.New().AddStream(normalVideo).SetOutput(normalFileName)
                    .AddParameter(arguments.Scale.ToString())
                    .AddParameter(arguments.FPS)
                    .AddParameter(arguments.Vf == "-vf " ? "" : arguments.Vf)
                    .AddParameter(arguments.Duration);

                IConversionResult normalResult = await normalConvers.Start();

                if (!normalResult.Success)
                {
                    throw new Exception(normalResult.Arguments);
                }

                // Конверсия файла с инверсией.
                IMediaInfo normalFileMediaInfo = await MediaInfo.Get(normalFileName);
                IVideoStream reverseVideo = normalFileMediaInfo.VideoStreams.FirstOrDefault().Reverse();
                IConversion reverseConvers = Conversion.New().AddStream(reverseVideo).SetOutput(reverseFileName);

                IConversionResult reverseResult = await reverseConvers.Start();

                if (!reverseResult.Success)
                {
                    throw new Exception(reverseResult.Arguments);
                }

                // Объединяем файлы.
                string parameters =
                    $"-i {WrapQuotes(normalFileName)} -i {WrapQuotes(reverseFileName)} -n -threads 4"
                    + $" -filter_complex \"[0:v:0] [1:v:0] concat=n=2:v=1 [v]\" -map \"[v]\" {WrapQuotes(concatFileName)}";

                IConversionResult concatResult = await new Conversion().Start(parameters);

                if (!concatResult.Success)
                {
                    throw new Exception(concatResult.Arguments);
                }

                // Создаем палитру.
                parameters = $"-i {WrapQuotes(concatFileName)} -n -threads {Environment.ProcessorCount}"
                    +  $" -vf palettegen {WrapQuotes(paletteFileName)}";

                IConversionResult paletteResult = await new Conversion().Start(parameters);

                if (!paletteResult.Success)
                {
                    throw new Exception(paletteResult.Arguments);
                }

                // Конвертируем gif, используя палитру.
                parameters =
                    $"-i {WrapQuotes(concatFileName)} -i {WrapQuotes(paletteFileName)} -y -threads {Environment.ProcessorCount}" 
                    + $" -filter_complex \"[0][1]paletteuse\" {WrapQuotes(saveFileName)}";

                IConversionResult gifResult = await new Conversion().Start(parameters);

                stopWatch.Stop();

                if (!gifResult.Success)
                {
                    throw new Exception(gifResult.Arguments);
                }

                FileInfo fileInfo = new FileInfo(saveFileName);

                OnConvertSuccess(new ConvertSuccessEventArgs(stopWatch.Elapsed, startTime, fileInfo));
            }
            catch (Exception e)
            {
                stopWatch.Stop();

                if (!savedFileExists)
                {
                    File.Delete(saveFileName);
                }

                OnConvertError(new ConvertErrorEventArgs(e.Message, stopWatch.Elapsed, startTime));
            }
            finally
            {
                File.Delete(normalFileName);
                File.Delete(reverseFileName);
                File.Delete(concatFileName);
                File.Delete(paletteFileName);
            }
        }

        /// <summary>
        /// Уведомляет зарегестрированные объекты события <see cref="ConvertError"/>.
        /// </summary>
        /// <param name="e">Аргументы ошибки конвертации.</param>
        private void OnConvertError(ConvertErrorEventArgs e)
        {
            Volatile.Read(ref ConvertError)?.Invoke(this, e);
        }

        /// <summary>
        /// Уведомляет зарегестрированные объекты события <see cref="ConvertSuccess"/>.
        /// </summary>
        /// <param name="e">Аргументы успешной конвертации.</param>
        private void OnConvertSuccess(ConvertSuccessEventArgs e)
        {
            Volatile.Read(ref ConvertSuccess)?.Invoke(this, e);
        }

        private string WrapQuotes(string str)
        {
            return $"\"{str}\"";
        }
    }

    /// <summary>
    /// Аргументы ошибки конвертации.
    /// </summary>
    public class ConvertErrorEventArgs : EventArgs
    {
        public string Message { get; }
        public TimeSpan Duration { get; }
        public TimeSpan StartTime { get; }

        public ConvertErrorEventArgs(string message, TimeSpan duration
            , TimeSpan startTime)
        {
            this.Message = message;
            this.Duration = duration;
            this.StartTime = startTime;
        }
    }

    /// <summary>
    /// Аргументы успешной конвертации.
    /// </summary>
    public class ConvertSuccessEventArgs : EventArgs
    {
        public TimeSpan Duration { get; }
        public TimeSpan StartTime { get; }
        public FileInfo SavedFile { get; }

        public ConvertSuccessEventArgs(TimeSpan duration
            , TimeSpan startTime, FileInfo savedFile)
        {
            this.Duration = duration;
            this.StartTime = startTime;
            this.SavedFile = savedFile;
        }
    }

    /// <summary>
    /// Аргументы для FFmpeg.
    /// </summary>
    public class FFmpegArgs
    {
        public class ScaleArg
        {
            public ScaleArg(int width, int height)
            {
                this.Width = width >= 0 ? width : 0;
                this.Height = height >= 0 ? height : 0;
            }

            private readonly int Width;
            private readonly int Height;

            public override string ToString()
            {
                return $"-s {this.Width}x{this.Height}";
            }
        }

        string _vf;

        /// <summary>
        /// Аргумент "-vf".
        /// </summary>
        public string Vf
        {
            get => _vf;
            set
            {
                _vf = $"-vf {value}";
            }
        }

        string _duration;

        /// <summary>
        /// Аргумент "-t".
        /// </summary>
        public string Duration
        {
            get => _duration;
            set
            {
                _duration = $"-t {value}";
            }
        }

        string _fps;

        /// <summary>
        /// Аргумент "-r".
        /// </summary>
        public string FPS
        {
            get => _fps;
            set
            {
                _fps = $"-r {value}";

            }
        }

        /// <summary>
        /// Аргумент "-s".
        /// </summary>
        public ScaleArg Scale { get; set; }

        public override string ToString()
        {
            return $"{Duration} {FPS} {Scale} {Vf}";
        }
    }
}
